import { Module } from "@nestjs/common";
import { ChatGateway } from "./gateways/test.gateway";
import { GoModule } from "../go.module";

@Module({
    imports: [GoModule],
    providers: [ChatGateway],
})
export class GoSockets {}