import { Table, Column, Model, HasMany, HasOne, BelongsToMany } from "sequelize-typescript";
import { Message } from "./message.model";
import { User } from "../users/user.model";
import { UserChats } from "./userchats.model";
import { Event } from "../events/event.model";


@Table
export class Chat extends Model {

    @Column
    title: string;

    @Column
    description: string;

    @HasMany(() => Message)
    messages: Message[];

    @HasOne(() => Event)
    event: Event;

    @BelongsToMany(() => User, () => UserChats)
    users: User[]

}