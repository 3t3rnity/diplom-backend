import { Table,Column, Model, BelongsTo, ForeignKey } from "sequelize-typescript";
import { Chat } from "./chat.model";

@Table({ updatedAt: false })
export class Message extends Model {

    @Column
    text: string;

    @ForeignKey(() => Chat)
    @Column
    chatId: number;
}