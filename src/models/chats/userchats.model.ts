import { Table, Column, Model, ForeignKey } from 'sequelize-typescript';
import { Chat } from './chat.model';
import { User } from '../users/user.model';

@Table({ updatedAt: false })
export class UserChats extends Model {

    @ForeignKey(() => Chat)
    @Column
    chatId: number;

    @ForeignKey(() => User)
    @Column
    userId: number;

}