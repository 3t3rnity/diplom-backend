import { Column, Table, Model, ForeignKey } from "sequelize-typescript";
import { Tags } from "./tags.model";
import { User } from "../users/user.model";

@Table({ createdAt: false })
export class UserTags extends Model {

    @ForeignKey(() => User)
    @Column
    userId: number;

    @ForeignKey(() => Tags)
    @Column
    tagId: number;  

}