import {
  MessageBody,
  ConnectedSocket,
  SubscribeMessage,
  WebSocketGateway,
  WebSocketServer,
  OnGatewayConnection,
} from '@nestjs/websockets';
import { Server, Socket } from 'socket.io';
import { UsersService } from 'src/GoModule/src/User/user.service';
import { AuthService } from 'src/GoModule/src/Auth/auth.service';
import { ChatsService } from 'src/GoModule/src/Chat/chat.service';

@WebSocketGateway({ cors: true })
export class ChatGateway implements OnGatewayConnection<any> {
  @WebSocketServer()
  server: Server;

  constructor(
    private UsersService: UsersService,
    private AuthService: AuthService,
    private ChatsService: ChatsService
  ) {}

  async handleConnection(@ConnectedSocket() socket: Socket) {
    console.log('SOCKET_SERVER');
  }

  @SubscribeMessage('hello')
  async listenForMessages(
    @MessageBody() data: any,
    @ConnectedSocket() socket: Socket
  ) {
    try {
      const user = socket.handshake.auth;
      this.server.sockets.emit('hello', {
        user: user.firstName,
        msg: data.msg,
      });
      this.server.sockets.emit('error', { text: 'TESTING' });
    } catch (e) {
      console.log(e);
    }
  }

  @SubscribeMessage('chatConnect')
  async chatConnect(@MessageBody() data: any, @ConnectedSocket() socket: any) {
    try {
      const user = socket.handshake.auth;
      const request = await this.ChatsService.joinChat({
        userId: user.id,
        chatId: data.chatId,
      });
      socket.join(`chat_id_${data.chatId}`);
      console.log(request);
    } catch (e) {
      console.log(e);
    }
  }

  @SubscribeMessage('chatDisconnect')
  async chatDisconnect(
    @MessageBody() data: any,
    @ConnectedSocket() socket: any
  ) {
    try {
      const user = socket.handshake.auth;
      const request = await this.ChatsService.leaveChat({
        userId: user.id,
        chatId: data.chatId,
      });
      socket.leave(`chat_id_${data.chatId}`);
      console.log(request);
    } catch (e) {
      console.log(e);
    }
  }

  @SubscribeMessage('chatMsg')
  async chatMsg(@MessageBody() data: any, @ConnectedSocket() socket: any) {
    try {
      const user = socket.handshake.auth;
      this.server.sockets.to(`chat_id_${data.chatId}`).emit('chatMsg', {
        user: user.firstName,
        msg: data.msg,
      });
    } catch (e) {
      console.log(e);
    }
  }
}
