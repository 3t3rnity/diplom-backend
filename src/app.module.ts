import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { SequelizeModule } from '@nestjs/sequelize';
import { GoModule } from './GoModule/go.module';
import { GoSockets } from './GoModule/sockets/go_sockets.module';
@Module({
  imports: [
    ConfigModule.forRoot(),
    SequelizeModule.forRoot({
      dialect: 'mysql',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: 'root',
      database: 'platform_db',
      models: ["dist/models/**/*.model.{.ts,.js}"],
      autoLoadModels: true,
      synchronize: true
    }),
    GoModule,
    GoSockets
  ],
})
export class AppModule {}
