import { Column, Table, Model, ForeignKey } from "sequelize-typescript";
import { User } from "../users/user.model";

@Table
export class UserFriends extends Model {

    @ForeignKey(() => User)
    @Column
    userId: number;

    @ForeignKey(() => User)
    @Column
    friendId: number;  

}