import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Role } from '../../../models/roles/role.model';

@Injectable()
export class RolesService {
    constructor(
        @InjectModel(Role)
        private roleModel: typeof Role
    ) {}

    async createRole(dto): Promise<Role> {
        const role = this.roleModel.create(dto)
        return role
    }

    async findAll(): Promise<Role[]> {
        return this.roleModel.findAll({ include: { all: true } })
    }

    findOne(id: string): Promise<Role> {
        return this.roleModel.findOne({
          where: {
            id,
          },
          include: {all: true}
        });
      }
    
      async updateRole(id: string, update_dto: object): Promise<Role> {
        const role = await this.findOne(id)
        role.update(update_dto)
        return role;
      }
    
      async remove(id: string): Promise<void> {
        const role = await this.findOne(id);
        await role.destroy();
      }
}