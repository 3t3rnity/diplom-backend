import { Controller, Body, Get, Post, Delete, Param } from '@nestjs/common';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('login')
  login(@Body() dto) {
    try {
        const user = this.authService.login(dto);
        return user
    } catch(e) {
        return e
    }
  }

  @Post('registration')
  registration(@Body() dto) {
    try {
      const user = this.authService.registration(dto);
      return user;
    } catch (e) {
      return e;
    }
  }

  @Post('test')
  test(@Body() token) {
      return this.authService.encodeToken(token)
  }
}
