import {
  HttpException,
  HttpStatus,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../User/user.service';
import { User } from 'src/models/users/user.model';
import * as bcrypt from 'bcryptjs';

@Injectable()
export class AuthService {
  constructor(
    private userService: UsersService,
    private jwtService: JwtService
  ) {}

  async login(userDto): Promise<any> {
    try {
      const user = await this.validateUser(userDto);
      return this.generateToken(user);
    } catch (e) {
      throw new UnauthorizedException({ msg: 'Неверные почта или пароль' });
    }
  }

  async registration(userDto) {
    const candidate = await this.userService.findByMail(userDto.mail);
    if (candidate)
      throw new HttpException(
        'Пользователь с таким email уже существует',
        HttpStatus.BAD_REQUEST
      );
    try {
      const hashPassword = await bcrypt.hash(userDto.password, 7);
      const user = await this.userService.createUser({
        ...userDto,
        password: hashPassword,
      });
      return this.generateToken(user);
    } catch (e) {
      return e;
    }
  }

  private async generateToken(user: User) {
    const payload = {
      id: user.id,
      roleId: user.roleId,
      mail: user.mail,
      firstName: user.firstName,
      lastName: user.lastName,
    };
    return {
      token: this.jwtService.sign(payload),
    };
  }

  private async validateUser(userDto) {
    const user = await this.userService.findByMail(userDto.mail);
    const passwordEquals = await bcrypt.compare(
      userDto.password,
      user.password
    );
    if (user && passwordEquals) {
      return user;
    }
  }

  async encodeToken(dto) {
      const user = await this.jwtService.decode(dto.token)
      return user
  }

}
