import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { JwtModule } from '@nestjs/jwt';
import { GoSockets } from './sockets/go_sockets.module';
import { UsersController } from './src/User/user.controller';
import { UsersService } from './src/User/user.service';
import { User } from 'src/models/users/user.model';
import { Tags } from 'src/models/tags/tags.model';
import { UserTags } from 'src/models/tags/usertags.model';
import { Message } from 'src/models/chats/message.model';
import { Chat } from 'src/models/chats/chat.model';
import { UserChats } from 'src/models/chats/userchats.model';
import { Event } from 'src/models/events/event.model';
import { UserEvents } from 'src/models/events/userevents.model';
import { EventTags } from 'src/models/tags/eventtags.model';
import { Role } from 'src/models/roles/role.model';
// import { UserRoles } from 'src/models/roles/userroles.model';
import { RolesController } from './src/Roles/roles.controller';
import { RolesService } from './src/Roles/roles.service';
import { AuthService } from './src/Auth/auth.service';
import { AuthController } from './src/Auth/auth.controller';
import { ChatsService } from './src/Chat/chat.service';
import { ChatController } from './src/Chat/chat.controller';

export const models = SequelizeModule.forFeature([
  User,
  Role,
  Tags,
  UserTags,
  EventTags,
  UserChats,
  UserEvents,
  // UserRoles,
  Chat,
  Message,
  Event,
  
])
@Module({
  imports: [
    models,
    JwtModule.register({
      secret: 'secret_key_catsgoin',
      signOptions: {
        expiresIn: '1h'
      }
    })
  ],
  controllers: [UsersController, RolesController, AuthController,ChatController],
  providers: [UsersService, RolesService, AuthService, ChatsService],
  exports: [UsersService, AuthService, ChatsService]
})
export class GoModule {}
