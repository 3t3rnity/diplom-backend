import { Column, Table, Model, ForeignKey } from "sequelize-typescript";
import { Tags } from "./tags.model";
import { Event } from "../events/event.model";

@Table({ createdAt: false })
export class EventTags extends Model {

    @ForeignKey(() => Event)
    @Column
    eventId: number;

    @ForeignKey(() => Tags)
    @Column
    tagId: number;  

}