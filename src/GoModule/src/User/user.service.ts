import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { User } from '../../../models/users/user.model';
@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User)
    private userModel: typeof User
  ) {}

  async createUser(dto: any): Promise<User> {
    const user = await this.userModel.create(dto);
    return user
  }

  async updateUserTags(dto: any): Promise<User> {
    const user = await this.findOne(dto.userId)
    await user.$set('usertags', [dto.tags])
    return user
  }

  async findAll(): Promise<User[]> {
    return this.userModel.findAll({ include: { all: true } });
  }

  findOne(id: string): Promise<User> {
    return this.userModel.findOne({
      where: {
        id,
      },
      include: {all: true}
    });
  }

  async findByMail(mail: string): Promise<User> {
    return this.userModel.findOne({
      where: {
        mail
      },
      include: { all: true }
    })
  }

  async login(mail: string, password: string): Promise<User> {
    return this.userModel.findOne({
      where: {
        mail,
        password
      }
    })
  }

  async updateUser(id: string, update_dto: object): Promise<User> {
    const user = await this.findOne(id)
    user.update(update_dto)
    return user;
  }

  async remove(id: string): Promise<void> {
    const user = await this.findOne(id);
    await user.destroy();
  }
}
