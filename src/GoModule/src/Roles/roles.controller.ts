import { Controller, Body, Get, Post } from '@nestjs/common';
import { RolesService } from './roles.service';

@Controller('roles')
export class RolesController {
  constructor(private rolesService: RolesService) {}

  @Post('createRole')
  async createRole(@Body() dto) {
    try {
      console.log('RoleDTO: ', dto);
      const role = await this.rolesService.createRole(dto);
      console.log('Controller', role)
      return role;
    } catch (e) {
      console.log('Controller', e)
      return { success: false, e };
    }
  }

  @Post('updateRole')
  async updateRole(@Body() dto) {
    try {
      const role = await this.rolesService.updateRole(dto.id, dto.body);
      return role;
    } catch (e) {
      return { success: false, e };
    }
  }

  @Get('findAll')
  async findRoles() {
    try {
      const roles = await this.rolesService.findAll();
      return roles;
    } catch (e) {
      return { success: false, e };
    }
  }
}