import { Controller, Body, Get, Post } from '@nestjs/common';
import { ChatsService } from './chat.service';

@Controller('chat')
export class ChatController {
  constructor(private chatService: ChatsService) {}

  @Post('createChat')
  async createChat(@Body() dto) {
    try {
      console.log('ChatDTO: ', dto);
      const chat = await this.chatService.createChat(dto);
      console.log('Controller', chat);
      return chat;
    } catch (e) {
      console.log('Controller', e);
      return { success: false, e };
    }
  }

  @Get('findAll')
  async findAllChats() {
    try {
      const chats = await this.chatService.findAll();
      return chats;
    } catch (e) {
      console.log(e);
    }
  }

  @Post('findOne')
  async findOneChat(@Body() dto) {
    try {
      const chat = await this.chatService.findOne(dto.chatId);
      return chat;
    } catch (e) {
      console.log(e);
    }
  }
}
