import {
  Column,
  Model,
  Table,
  Validate,
  BelongsToMany,
  ForeignKey,
} from 'sequelize-typescript';
import { Tags } from '../tags/tags.model';
import { UserTags } from '../tags/usertags.model';
import { Chat } from '../chats/chat.model';
import { UserChats } from '../chats/userchats.model';
import { Event } from '../events/event.model';
import { UserEvents } from '../events/userevents.model';
import { Role } from '../roles/role.model';
import { UserFriends } from './userfriends.model';
// import { UserRoles } from '../roles/userroles.model';

@Table
export class User extends Model {
  @Validate({ notEmpty: true })
  @Column({ allowNull: false })
  firstName: string;

  @Validate({ notEmpty: true })
  @Column({ allowNull: false })
  lastName: string;

  @Validate({ isEmail: true })
  @Column({ allowNull: false })
  mail: string;

  @Validate({ notEmpty: true, len: [6, 100] })
  @Column({ allowNull: false })
  password: string;

  @Column({ defaultValue: true })
  isActive: boolean;

  @ForeignKey(() => Role)
  @Column({ defaultValue: 2 })
  roleId: number;

  @BelongsToMany(() => Event, () => UserEvents)
  events: Event[];

  @BelongsToMany(() => Chat, () => UserChats)
  userchats: Chat[];

  @BelongsToMany(() => Tags, () => UserTags)
  usertags: Tags[];
}
