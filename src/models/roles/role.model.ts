import { Table, Column, Model, HasMany, Validate } from "sequelize-typescript";
import { User } from "../users/user.model";
// import { UserRoles } from "./userroles.model";


@Table
export class Role extends Model {

    @Validate({ notEmpty: true })
    @Column({ allowNull: false })
    value: string;

    @Validate({ notEmpty: true })
    @Column({ allowNull: false })
    title: string;

    @HasMany(() => User)
    users: User[]
    
}
