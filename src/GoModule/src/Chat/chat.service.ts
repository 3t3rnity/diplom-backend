import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { Op } from 'sequelize';
import { UsersService } from '../User/user.service';
import { User } from 'src/models/users/user.model';
import { Chat } from 'src/models/chats/chat.model';
import { UserChats } from 'src/models/chats/userchats.model';

@Injectable()
export class ChatsService {
  constructor(
    @InjectModel(User)
    private userModel: typeof User,
    @InjectModel(Chat)
    private chatModel: typeof Chat,
    private usersService: UsersService
  ) {}

  async createChat(dto): Promise<any> {
    const user = await this.usersService.findOne(dto.userId);
    let chat = await Chat.create({
      title: dto.title,
      description: dto.description,
    });
    await chat.$add('users', [user]);
    console.log('SERVICE: ', user);
    return chat;
  }

  async joinChat(dto): Promise<any> {
    const user = await this.usersService.findOne(dto.userId)
    const chat = await this.chatModel.findOne({
      where: {
        id: dto.chatId
      }
    })
    chat.$add('users', user)
    return `User ${dto.userId} joined the chat ${dto.chatId}`
  }

  async leaveChat(dto): Promise<any> {
    const user = await this.usersService.findOne(dto.userId)
    const chat = await this.chatModel.findOne({
      where: {
        id: dto.chatId
      }
    })
    chat.$remove('users', user)
    return `User ${dto.userId} leaved the chat ${dto.chatId}`
  }

  async findAll(): Promise<Chat[]> {
    return this.chatModel.findAll({ include: { all: true } });
  }

  async findOne(chatId: number): Promise<Chat> {
    return this.chatModel.findOne({
      where: { id: chatId },
      include: { all: true },
    });
  }
}
