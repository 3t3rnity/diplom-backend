import { Injectable } from "@nestjs/common";
import { InjectModel } from "@nestjs/sequelize";
import { Tags } from "src/models/tags/tags.model";

@Injectable()
export class TagsService {
    constructor(
        @InjectModel(Tags)
        private tagsModel: typeof Tags
    ) {}

    async createTag(dto): Promise<Tags> {
        const tag = this.tagsModel.create(dto)
        return tag
    }

    async findAll(): Promise<Tags[]> {
        return this.tagsModel.findAll({ include: { all: true } })
    }

    findOne(id: string): Promise<Tags> {
        return this.tagsModel.findOne({
          where: {
            id,
          },
          include: {all: true}
        });
      }
    
      async updateTag(id: string, update_dto: object): Promise<Tags> {
        const tag = await this.findOne(id)
        tag.update(update_dto)
        return tag;
      }
    
      async remove(id: string): Promise<void> {
        const tag = await this.findOne(id);
        await tag.destroy();
      }
}