import { Table, Column, Model, BelongsToMany } from "sequelize-typescript";
import { User } from "../users/user.model";
import { UserTags } from "./usertags.model";
import { Event } from "../events/event.model";
import { EventTags } from "./eventtags.model";


@Table
export class Tags extends Model {

    @Column
    text: string;

    @Column
    color: string;

    @BelongsToMany(() => User, () => UserTags)
    users: User[];

    @BelongsToMany(() => Event, () => EventTags)
    events: Event[];
}