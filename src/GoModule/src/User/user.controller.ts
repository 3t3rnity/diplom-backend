import { Controller, Body, Get, Post, Delete, Param } from '@nestjs/common';
import { UsersService } from './user.service';

@Controller('user')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Post('createUser')
  async createUser(@Body() dto) {
    try {
      const user = await this.usersService.createUser(dto);
      return user;
    } catch (e) {
      return { success: false, e };
    }
  }

  @Post('updateUser')
  async updateUser(@Body() dto) {
    try {
      const user = await this.usersService.updateUser(dto.id, dto.body);
      return user;
    } catch (e) {
      return { success: false, e };
    }
  }

  @Delete('removeUser')
  async removeUser(@Body() dto) {
    try {
      await this.usersService.remove(dto.userId)
      return { success: true, msg: 'User has been removed' }
    } catch (e) {
      return { success: false, e }
    } 
  }

  @Post('loginUser')
  async loginUser(@Body() dto) {
    try {
      const user = await this.usersService.login(dto.mail, dto.password)
      return user
    } catch(e) {
      return { success: false, e }
    }
  }

  @Get('findUser/:id')
  async findUser(@Param() params) {
    try {
      console.log('find', params.id)
      const user = await this.usersService.findOne(params.id)
      console.log('finded', user)
      return user
    } catch (e) {
      return { success: false, e }
    }
  }

  @Get('findall')
  async findUsers() {
    try {
      const users = await this.usersService.findAll();
      return users;
    } catch (e) {
      return { success: false, e };
    }
  }


}
