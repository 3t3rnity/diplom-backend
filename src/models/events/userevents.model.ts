import { Column, Table, Model, ForeignKey } from "sequelize-typescript";
import { Event } from "./event.model";
import { User } from "../users/user.model";

@Table({ createdAt: false })
export class UserEvents extends Model {

    @ForeignKey(() => User)
    @Column
    userId: number;

    @ForeignKey(() => Event)
    @Column
    eventId: number;  

}