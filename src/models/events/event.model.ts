import { Table, Column, Model, BelongsToMany, HasOne, ForeignKey } from "sequelize-typescript";
import { Chat } from "../chats/chat.model";
import { User } from "../users/user.model";
import { UserEvents } from "./userevents.model";
import { Tags } from "../tags/tags.model";
import { EventTags } from "../tags/eventtags.model";

@Table
export class Event extends Model {

    @Column
    title: string;
    
    @Column
    description: string;

    @BelongsToMany(() => Tags, () => EventTags)
    tags: Tags[];

    @BelongsToMany(() => User, () => UserEvents)
    userId: number;

    @ForeignKey(() => Chat)
    chatId: number;
}